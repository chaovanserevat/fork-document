				</div>
			</div>
			<div id="bottom">
				<div style="font-size: 12px;">
					<span style="margin-right: 50px;"><?= $today ?></span>
					Welcome <b><?= $user_fullname ?></b>! | <a href="<?= base_url() ?>logout.php">Logout</a>
				</div>
				<p style="color: #777777;">Nked - Document Management Power by <a href="http://sursdey.com">bv<a></p>
			</div>
		</div>
		<script type="text/javascript">

			jQuery.extend({
				ajax_loading: function(args) {
					var loading = $(".ajax-loading").clone()
						.attr("class", "ajax-loading-clone").show();
					if (!args) return loading;
					else if (args == "hide") {
						$(".ajax-loading-clone").remove();
					}
				}
			});

			$(function() {

				$("#middle").middleHeight();
				$("#content").load("content/document/document.php");

				$(".menu-top").click(function() {
					var id = $(this).attr("id");
					$("#content").load("content/" + id + "/" + id + ".php");
				});


				$.fn.existx = function(){ return this.length > 0; }

				$.fn.clearForm = function() {
					return this.each(function() {
						var type = this.type, tag = this.tagName.toLowerCase();

						if (tag == "form")
							return $(":input", this).clearForm();
						if (type == "text" || type == "password" || tag == "textarea")
							this.value = "";
						else if (type == "checkbox" || type == "radio")
							this.checked = false;
						else if (tag == "select")
							this.selectedIndex = -1;
					});
				};

				$.fn.tickCheckbox = function() {
					var _checkbox = $(".nk-tbl-data input:checkbox");
					_checkbox.addClass("fd-ckb");

					return _checkbox.each(function() {
						$(this).closest("tr").addClass("tr-fd-ckb");
					});
				}

				$.fn.ifCheck = function() {
					var _checkbox = $(".fd-ckb:checked");

					$(".tr-fd-ckb").removeClass("fd-ckb-cked");

					return _checkbox.each(function() {
						$(this).closest("tr").addClass("fd-ckb-cked");
					});
				}

				$(".tr-fd-ckb").live("click", function() {
					var _checkbox = $(this).find(".fd-ckb");

					var _status = _checkbox.attr("checked") ? false : true;

					_checkbox.attr("checked", _status);

					$.ifCheck();
				});

				$(".fd-ckb").live("click", function() {
					var _status = $(this).attr("checked") ? false : true;
					$(this).attr("checked", _status);

					$.ifCheck();
				});

				$.fn.noSpecialChar = function() {

					return $(this).each (function() {

						$(this).addClass("input-no-special-char");

					});

				}

				var _old_ = "";

				$(".input-no-special-char, .no-special-char").live("keyup", function() {
					var inputVal = $(this).val();
					var characterReg = /^[a-zA-Z0-9_]*$/;

					if(!characterReg.test(inputVal)) {
						$(this).val(_old_);
						alert("No special characters allowed.");
					}
				})
				.live("keydown", function() {
					_old_ = $.trim ($(this).val());
				});

				$.fn.checkRequiredx = function() {

					var _form = $(".formx-entry");

					return _form.each(function() {

						_requiredx = [];

						$(this).find(".requiredx").each(function(i) {
							if ($(this).val() == "") {
								_requiredx[i] = $(this);
								$(this).addClass("requiredx-needed");
							}
							else $(this).removeClass("requiredx-needed");
						});

						if (_requiredx.length > 0) {
							return false;
						}
					});

				}
				
				$.fn.validatex = function(event) {
				
					$.extend({
						checkRequiredx: $.fn.checkRequiredx
					});
					
					$.checkRequiredx();
					
					if ($(".requiredx-needed").existx() > 0) {
						$(".requiredx-needed[value='']:first").focus();
						// event.stopImmediatePropagation();
						return false;
					}
					
				}

				$.extend({
					tickCheckbox: $.fn.tickCheckbox,
					ifCheck: $.fn.ifCheck,
					locationx: $.fn.locationx,
					validatex: $.fn.validatex
				});
				
				$(".focus").live("focus", function() {
					$(".focus").removeClass("focus-color");
					$(this).addClass("focus-color")
				});

			});

			$(window).resize(function() {
				$("#middle").middleHeight();
			});

		</script>
	</body>
</html>