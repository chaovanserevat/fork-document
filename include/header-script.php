<?php

$logout = isset ($_SESSION["user-logout"]) ? $_SESSION["user-logout"] : -1;

if ($_SESSION['user-login'] < 1 || $_SESSION['user-login'] == -1 || $logout == 1) {
	$_SESSION["user-logout"] = -1;
	header ("location: login.php");
	exit ();
}

$user_fullname = $_SESSION["fullname"];
$today = date("F j, Y, g:i a");

$select_module = $connection->query ("SELECT * FROM tbl_module");
$modules = array ();
while ($get_module = $select_module->fetch_assoc ()) {
	$modules[] = $get_module;
}

?>