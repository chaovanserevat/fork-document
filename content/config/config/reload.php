<?php

include ("../../../connection.php");
$select = $connection->query("SELECT * FROM tbl_standee");
	$standees = array ();
	while ($standee = $select->fetch_assoc()) :
		$standees[] = $standee;
	endwhile;

?>
<? if (COUNT($standees) > 0) : ?>
<? foreach ($standees as $standee) : ?>
<tr>
    <td><input type="checkbox" value="<?= $standee["id"] ?>" /></td>
    <td><?= $standee["number"] ?></td>
    <td><?= $standee["description"] ?></td>
    <td>
        <a class="link-edit" href="#<?= $standee["id"]; ?>">
            <img src="img/edit.png" alt="" />
        </a>
    </td>
    <td>
        <a class="link-delete" href="#<?= $standee["id"]; ?>">
            <img src="img/delete.png" alt="" />
        </a>
    </td>
</tr>
<? endforeach; ?>
<? else : ?>
<tr>
    <td colspan="4" style="color: red;">No Standee Found!</td>
</tr>
<? endif ?>