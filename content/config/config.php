<style type="text/css">

.link-edit img, .link-delete img {
	width: 16px;
	height: 16px;
}

#tbl-standee tbody tr:nth-child(odd) { background: #EDF7F8 }
#tbl-standee tbody tr:nth-child(even) { background: #F8F9ED }
#tbl-standee tbody > tr:hover { background: #FFF }

</style>

<div>
	<p>
		<button id="nk-btn-new-standee">New Standee</button>
	</p>
	
	<div>
		<center>
			<p style="color: blue;">Standee are showing all :</p>
			<table id="tbl-standee" class="nk-tbl-data ui-corner-all">
				<col style="width: 10px;" />
				<col style="width: 350px;" />
				<col style="width: 580px;" />
				<col style="width: 10px;" />
				<col style="width: 10px;" />
				<thead>
					<tr class="ui-widget-header">
						<th class="align-left">#No</th>
						<th>Standee Number</th>
						<th>Standee Description</th>
						<th colspan="2">&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="align-center ui-state-error" colspan="5">Standee not found!</td>
					</tr>	
				</tbody>
			</table>
		</center>
	</div>
<div>

<script type="text/javascript">

$("button").button();

$("#tbl-standee tbody").load("content/standee/standee/reload.php");

/* create new user account *********************************************/

$("#nk-btn-new-standee").click(function() {
    $("<div></div>", {
        id: "dlg-standee-new",
        title: "New Standee"
    })
    .dialog({
        width: 520, height: 365,
        modal: true, resizable: false,
        close: function() {
            $(this).dialog("destroy").remove();
        },
        buttons: {
            Save: function() {
                save_standee();
                $(this).dialog("close");
            },
            Close: function() {
                $(this).dialog("close");
            }
        }
    })
    .html(
        $("<center></center>").html(
            $.ajax_loading().css({ "padding-top": 5 })
        )
    )
    .load("content/standee/standee/addnew.php");
});

/* ********************************************************************** */

/* edit user account ******************************************************/

$("#tbl-standee").delegate(".link-edit", "click", function() {
    var id = $(this).attr("href").split("#");
	
    $("<div></div>", {
        id: "dlg-standee-edit",
        title: "Edit Standee"
    })
    .dialog({
        modal: true, resizable: false,
        width: 520, height: 440,
        close: function() {
            $(this).dialog("destroy").remove();
        },
        buttons: {
            Update: function() {
                save_standee();
            },
            Close: function() {
                $(this).dialog("close");
            }
        }
    })

    .html(
        $("<center></center>").html(
            $.ajax_loading().css({ "padding-top": 5 })
        )
    )
    .load("content/standee/standee/edit.php", {
        id: id[1]
    });
    return false; 
	
});

/* *************************************************************************/

/* delete */

$("#tbl-standee").delegate(".link-delete", "click", function() {
    var id = $(this).attr("href").split("#");
    $("<div></div>", {
        id: "dlg-standee-delete",
        title: "Delete Standee"
    })
    .html("Are you sure you want to delete this standee?")
    .dialog({
        modal: true, resizable: false,
        width: 350, height: 160,
        close: function() {
            $(this).dialog("destroy").remove();
        },
        buttons: {
            Yes: function() {
                $.ajax({
                    url: "content/standee/standee/delete.php",
                    type: "post", dataType: "html",
                    data: { id: id[1] },
                    success: function(num) {
                        $("#tbl-standee tbody").load("content/standee/standee/reload.php", function() {
                            if (num > 0) {
                                $("#table-status")
                                    .removeClass()
                                    .addClass("ui-state-error")
                                    .html("A user account has been deleted successfully!")
                                    .stop(true, true).fadeTo(0, 1).hide()
                                    .slideDown("normal").delay(2000).fadeOut("slow");
                            }
                        });
                        $("#dlg-standee-delete").dialog("close");
                    }
                });
            },
            No: function() {
                $(this).dialog("close");
            }
        }
    });
    return false;
});

/* *************************** */

</script>