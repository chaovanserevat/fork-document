<style type="text/css">

.link-edit img, .link-delete img {
	width: 16px;
	height: 16px;
}

#tbl-user-account tbody tr:nth-child(odd) { background: #EDF7F8 }
#tbl-user-account tbody tr:nth-child(even) { background: #F8F9ED }
#tbl-user-account tbody > tr:hover { background: #FFF }

fieldset legend { color: blue; }

</style>

<div>
	<p align="right">
		<button id="nk-btn-new-user">New User</button>
	</p>
	
	<div>
		<center>
			<p style="color: blue;">User Account are showing all accounts:</p>
			<table id="tbl-user-account" class="nk-tbl-data ui-corner-all">
				<col style="width: 10px;" />
				<col style="width: 300px;" />
				<col style="width: 300px;" />
				<col style="width: 330px;" />
				<col style="width: 10px;" />
				<col style="width: 10px;" />
				<thead>
					<tr class="ui-widget-header">
						<th class="align-left">#id</th>
						<th>Last Name</th>
						<th>First Name</th>
						<th>Account Name</th>
						<th colspan="2">&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="align-center ui-state-error" colspan="4">Data not found!</td>
					</tr>	
				</tbody>
			</table>
		</center>
	</div>
<div>

<script type="text/javascript">

$("button").button();

$("#tbl-user-account tbody").load("content/manage-user/account/reload.php");

/* create new user account *********************************************/

$("#nk-btn-new-user").click(function() {
    $("<div></div>", {
        id: "dlg-user-account-new",
        title: "New User Account"
    })
    .dialog({
        width: 1000, height: 630,
        modal: true, resizable: true,
        close: function() {
            $(this).dialog("destroy").remove();
        },
        buttons: {
            Save: function() {
                save_user_account();
                $(this).dialog("close");
            },
            Close: function() {
                $(this).dialog("close");
            }
        }
    })
    .html(
        $("<center></center>").html(
            $.ajax_loading().css({ "padding-top": 5 })
        )
    )
    .load("content/manage-user/account/addnew.php");
});

/* ********************************************************************** */

/* edit user account ******************************************************/

$("#tbl-user-account").delegate(".link-edit", "click", function() {
    var id = $(this).attr("href").split("#");
	console.log(id[1]);
    $("<div></div>", {
        id: "dlg-user-account-edit",
        title: "Edit User Account"
    })
    .dialog({
        modal: true, resizable: false,
        width: 520, height: 440,
        close: function() {
            $(this).dialog("destroy").remove();
        },
        buttons: {
            Save: function() {
                save_user_account();
            },
            Close: function() {
                $(this).dialog("close");
            }
        }
    })

    .html(
        $("<center></center>").html(
            $.ajax_loading().css({ "padding-top": 5 })
        )
    )
    .load("content/manage-user/account/edit.php", {
        id: id[1]
    });
    return false; 
	
});

/* *************************************************************************/

/* delete */

$("#tbl-user-account").delegate(".link-delete", "click", function() {
    var id = $(this).attr("href").split("#");
    $("<div></div>", {
        id: "dlg-user-account-delete",
        title: "Delete User Account"
    })
    .html("Are you sure you want to delete this user account?")
    .dialog({
        modal: true, resizable: false,
        width: 400, height: 160,
        close: function() {
            $(this).dialog("destroy").remove();
        },
        buttons: {
            Yes: function() {
                $.ajax({
                    url: "content/manage-user/account/delete.php",
                    type: "post", dataType: "html",
                    data: { id: id[1] },
                    success: function(num) {
						$("#tbl-user-account tbody").load("content/manage-user/account/reload.php");
                        // $("#tbl-user-account-data tbody").load("content/manage-user/account/reload.php", function() {
                            // if (num > 0) {
                                // $("#table-status")
                                    // .removeClass()
                                    // .addClass("ui-state-error")
                                    // .html("A user account has been deleted successfully!")
                                    // .stop(true, true).fadeTo(0, 1).hide()
                                    // .slideDown("normal").delay(2000).fadeOut("slow");
                            // }
                        // });
                        $("#dlg-user-account-delete").dialog("close");
                    }
                });
            },
            No: function() {
                $(this).dialog("close");
            }
        }
    });
    return false;
});

/* *************************** */

</script>