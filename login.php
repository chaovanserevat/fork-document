<?php

include ("connection.php");
session_start();
$_SESSION["user-logout"] = -1;

$type = isset ($_POST["type"]) ? $_POST["type"] : "";

if ($type != "") {

	# login
	$username = isset ($_POST["username"]) ? $_POST["username"] : "";
	$date = date ("Y-m-d h:m:s");

	if ($username == "") die ("Cannot ..");
	else {
		$login_password = isset ($_POST["password"]) ? $_POST["password"] : "";
		$password =  md5 ($login_password);
		$check_user = $connection->query ("SELECT username, fname, lname FROM tbl_user WHERE username = '$username' AND password = '$password'");
		
		if ($check_user->num_rows == 1) {
			$_SESSION["user-login"] = 1;
			$user = $check_user->fetch_assoc();
			$_SESSION["fullname"] = $user["lname"] . " " . $user["fname"];
			$_SESSION["invalid-login"] == 0;
			
			$get_user = $connection->query ("SELECT id FROM tbl_user WHERE username = '$username'")->fetch_assoc();
			$userid = $get_user["id"];
			
			$insert_action = $connection->query ("INSERT INTO tbl_activity SET user = '$userid', action = 'login', date = '$date', description = 'sucessfully login'");
			
			?>
			<script type="text/javascript"> parent.window.location.href = "index.php"; </script>
			<?

		}
		else {
			$_SESSION["user-login"] = -1;
			$message = "Bad login! Invalid Username or Password..";
			
			if (isset ($_SESSION["invalid-login"])) {
				$_SESSION["invalid-login"]++;
			}
			else $_SESSION["invalid-login"] = 1;
			
			$count_login = 4 - $_SESSION["invalid-login"];
			
			if ($_SESSION["invalid-login"] < 4) {
				if ($_SESSION["invalid-login"] == 1) $message .= "<span style='color: blue;'>(hint: $count_login times login left, nears blacklist..)</span>";
				elseif ($_SESSION["invalid-login"] == 2) $message .= "<span style='color: blue;'>(hint: <span style='color: red;'>take care!</span> $count_login times login left, nearly blacklist..)</span>";
				elseif ($_SESSION["invalid-login"] == 3) $message .= " <span style='color: blue;'>(hint: <span style='color: red;'>carefully!</span> $count_login times login left, block in blacklist..)</span>";				
				
				# insert action to activities
				$insert_action = $connection->query ("INSERT INTO tbl_activity SET user = '', action = 'login', date = '$date', description = 'user: $username and password: $login_password (login failed $count_login)'");
			
			}
			else {
				$max = $connection->query ("SELECT MAX(id) FROM tbl_activity")->fetch_assoc();
				$insert_action = $connection->query ("INSERT INTO tbl_blacklist SET user = '{$max["id"]}', date = '$date', description = 'user: $username and password: $login_password (blacklist account)'");
				
				?>
				<script type="text/javascript"> parent.window.location.href = "ged1e31a1a54g23ds1f21xc4asdfc4af32e1gghetert4e65t4213t2e1rtw5ert65wetg.php"; </script>
				<?
				
			}
						
			?>
			<script type="text/javascript"> parent.show_message("<?= $message ?>"); </script>
			<?
		}
	}

	exit ();
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

		<title>Document Center Login</title>

		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript">
		
			$(document).ready(function() {
				if (!($.browser.safari || $.browser.mozilla)) {
					$("form").hide();
					$("<div class='error' />").html("<h2>Browser Not Supported</h2>The Treemo Labs Dashboard utilizes cutting-edge browser technologies.<br /><br />We currently only support <ul><li>Firefox (version &gt; 3.5 recommended)</li><li>Safari (version &gt; 3 recommended)</li></ul>").appendTo('#login-content');
				}
				
				$("#txt-username").focus();
				
				$("#btn-login").click(function() {
					var required = [];
					$(".required").each(function(i) {
						if ($(this).val() == "") {
							$(this).addClass("error-required");
							required[i] = $(this);
						}
						else $(this).removeClass("error-required");
						
						if (required.length > 0) {
							return false;
						}
						
						$("form").submit();
						
					});
				});
				
			});
			
			function show_message(message) {
				$("#p-message").css("color", "red").html(message);
			}
			
		</script>

		<style type="text/css">
			* {
				font-family: "Lucida Console", "Helvetica Neue", Helvetica, Arial, sans-serif;
			}

			body {
				margin: 0;
				pading: 0;
				color: #fff;
				background: url('bg-login.png') repeat #1b1b1b;
				font-size: 14px;
				text-shadow: #050505 0 -1px 0;
				font-weight: bold;
			}

			li {
				list-style: none;
			}

			#dummy {
				position: absolute;
				top: 0;
				left: 0;
				border-bottom: solid 3px #777973;
				height: 250px;
				width: 100%;
				background: url('bg-login-top.png') repeat #fff;
				z-index: 1;
			}

			#dummy2 {
				position: absolute;
				top: 0;
				left: 0;
				border-bottom: solid 2px #545551;
				height: 252px;
				width: 100%;
				background: transparent;
				z-index: 2;
			}

			#login-wrapper {
				margin: 0 0 0 -210px;
				/*
				margin: 0 0 0 -160px;
				width: 320px;
				*/
				width: 668px;
				text-align: center;
				z-index: 99;
				position: absolute;
				top: 0;
				left: 50%;
			}

			#login-top {
				height: 85px;
				padding-top: 30px;
				padding-bottom: 95px;
				text-align: center;
			}

			label {
				width: 70px;
				float: left;
				padding: 8px;
				line-height: 14px;
				margin-top: -4px;
			}

			input.text-input {
				/*
				width: 200px;
				*/
				width: 280px;
				float: right;
				-moz-border-radius: 4px;
				-webkit-border-radius: 4px;
				border-radius: 4px;
				background: #fff;
				border: solid 1px transparent;
				color: #555;
				padding: 8px;
				font-size: 13px;
			}

			input.button {
				float: right;
				padding: 6px 10px;
				color: #fff;
				font-size: 14px;
				background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#a4d04a), to(#459300));
				text-shadow: #050505 0 -1px 0;
				background-color: #459300;
				-moz-border-radius: 4px;
				-webkit-border-radius: 4px;
				border-radius: 4px;
				border: solid 1px transparent;
				font-weight: bold;
				cursor: pointer;
				letter-spacing: 1px;
			}

			input.button:hover {
				background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#a4d04a), to(#a4d04a), color-stop(80%, #76b226));
				text-shadow: #050505 0 -1px 2px;
				background-color: #a4d04a;
				color: #fff;
			}

			div.error {
				padding: 8px;
				background: rgba(52, 4, 0, 0.4);
				-moz-border-radius: 8px;
				-webkit-border-radius: 8px;
				border-radius: 8px;
				border: solid 1px transparent;
				margin: 6px 0;
			}
			
			.error-required { border: 1px solid red; }
			
		</style>
	</head>

	<body id="">

		<div id="login-wrapper" class="png_bg">
			<div id="login-top" style="margin-left: -250px;">
				<!--
				<img src="./Treemo Labs Dashboard   Stats Center_files/treemoLabs-logo.png" alt="Treemo Labs Dashboard" title="Powered By Treemo Labs">
				-->
				<p style="color: rgb(222, 135, 69); font-size: 40px; font-weight: bold; margin-top: 0px;">Kingdom Of Cambodia</p>
				<span style="color: #DE8745; font-size: 40px; font-weight: bold">Document Management Centre</span>
			</div>
			
			<p id="p-message" style="width: 1030px; color: blue; font-size: 15px; margin-left: -300px;">Hint: Only 3 times for real login, then your account will block to blacklist</p>
			<div><img style="position: absolute; margin-left: -450px; margin-top: -10px;" src="img/user-login.gif" /></div>
			<div id="login-content" style="width: 400px; margin-left: 100px;">
				<p>Please Enter your real Username and Password</p>
				<br />

				<form action="login.php" method="post" target="hidden-iframe">
					<p>
						<label>Username</label>
						<input id="txt-username" value="" name="username" class="required text-input" type="text">
					</p>
					<br style="clear: both;">
					<p>
						<label>Password</label>
						<input id="txt-password" class="required text-input" name="password" type="password">
					</p>
					<br style="clear: both;">
					<input type="hidden" value="login" name="type">
				</form>
				<p><input id="btn-login" class="button" type="button" value="Sign In"></p>

				<iframe id="hidden-iframe" name="hidden-iframe" style="width: 0px; height: 0px; border: 0 none;"></iframe>
			</div>
		</div>
		<div id="dummy"></div>
		<div id="dummy2"></div>
	</body>
</html>